import React, { Component, Suspense } from "react";
// import axios from "axios";
import "./Blog.css";
// import Posts from "./Posts/Posts";
import { Route, NavLink, Switch, Redirect } from "react-router-dom";
import asyncComponent from "../../hoc/asyncComponent/asyncComponent";
// import NewPost from "../../containers/Blog/NewPost/NewPost";
const AsyncNewPost = asyncComponent(() => {
  return import("./NewPost/NewPost");
});
const Posts = React.lazy(() => import("./Posts/Posts"));

class Blog extends Component {
  state = {
    auth: true,
  };
  render() {
    return (
      <div>
        <header>
          <nav className="Blog">
            <ul>
              <li>
                {/* default value for activeClassName is "active" */}
                <NavLink
                  to="/posts"
                  exact
                  activeClassName="my-active"
                  activeStyle={{
                    color: "#fa923f",
                    textDecoration: "underline",
                  }}
                >
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={{
                    pathname: "/new-post",
                    hash: "#submit",
                    search: "?quick-submit=true",
                  }}
                >
                  New Post
                </NavLink>
              </li>
            </ul>
          </nav>
        </header>
        {/* <Route path="/" exact render={() => <h1>Home</h1>} />
        <Route path="/" render={() => <h1>Home 2</h1>} /> */}

        <Switch>
          {this.state.auth && (
            <Route path="/new-post" component={AsyncNewPost} />
          )}
          <Route
            path="/posts"
            component={() => (
              <Suspense fallback={<div>Loading...</div>}>
                <Posts />
              </Suspense>
            )}
          ></Route>
          <Route render={() => <h1>Not found</h1>} />
          {/* <Redirect from="/" to="posts" /> */}
        </Switch>
      </div>
    );
  }
}

export default Blog;
