import React, { Component } from "react";
import axios from "../../../axios.js";
import Post from "../../../components/Post/Post";
import Spinner from "../../../components/UI/Spinner/Spinner";
import { Link, Route } from "react-router-dom";
import FullPost from "../FullPost/FullPost";
import "./Posts.css";

class Posts extends Component {
  state = {
    posts: [],
    loading: false,
  };
  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get("/posts")
      .then((res) => {
        const posts = res.data.slice(0, 4);
        const updatePosts = posts.map((post) => ({
          ...post,
          author: "Topy",
        }));
        this.setState((prevState) => ({ posts: updatePosts, loading: false }));
      })
      .catch((err) => {
        console.log(err);
        this.setState({ loading: false });
        // this.setState({ error: true });
      });
  }

  postSelectedHandler = (id) => {
    // this.props.history.push({ pathname: "/" + id });
    this.props.history.push("/posts/" + id);
  };

  render() {
    let posts = <p style={{ textAlign: "center" }}>Something went wrong!</p>;
    if (!this.state.error) {
      posts = this.state.posts.map((item) => (
        // <Link key={item.id} to={"/posts/" + item.id}>
        <Post
          key={item.id}
          post={item}
          clicked={() => this.postSelectedHandler(item.id)}
        />
        // </Link>
      ));
    }
    if (this.state.loading) {
      posts = <Spinner />;
    }
    return (
      <div>
        <section className="Posts">{posts}</section>;
        <Route
          path={`${this.props.match.url}/:postId`}
          component={FullPost}
        ></Route>
      </div>
    );
  }
}

export default Posts;
