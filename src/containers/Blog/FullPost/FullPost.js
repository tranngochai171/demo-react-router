import React, { Component, useState, useEffect } from "react";
// import PropTypes from "prop-types";
import axios from "axios";
import Spinner from "../../../components/UI/Spinner/Spinner";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import "./FullPost.css";

class FullPost extends Component {
  state = {
    loadedPost: null,
    loading: false,
  };

  componentDidMount() {
    this.loadData();
  }
  componentDidUpdate() {
    this.loadData();
  }

  loadData = () => {
    if (this.props.match.params.postId) {
      if (
        !this.state.loadedPost ||
        (this.state.loadedPost &&
          this.state.loadedPost.id !== +this.props.match.params.postId)
      ) {
        axios
          .get("/posts/" + this.props.match.params.postId)
          .then((response) => {
            // console.log(response);
            this.setState({ loadedPost: response.data });
          });
      }
    }
  };

  // componentDidUpdate() {
  //   if (
  //     this.props.id &&
  //     (!this.state.loadedPost ||
  //       (this.state.loadedPost && this.state.loadedPost.id !== this.props.id))
  //   ) {
  //     axios.get(`/posts/${this.props.id}`).then((res) => {
  //       this.setState({ loadedPost: res.data });
  //     });
  //   }
  // }

  deletePostHandler = () => {
    axios.delete(`/posts/${this.props.match.params.postId}`).then((res) => {
      console.log(res);
    });
  };

  render() {
    const { loadedPost } = this.state;
    // let post = <p style={{ textAlign: "center" }}>Please select a Post!</p>;
    // if (this.props.id) {
    //   post = <p style={{ textAlign: "center" }}>Loading...</p>;
    // }

    // let post = loading ? (
    //   <Spinner style={{ textAlign: "center" }}>Loading...</Spinner>
    // ) : null;
    let post = <p style={{ textAlign: "center" }}>Please select a Post!</p>;
    if (loadedPost) {
      post = (
        <div className="FullPost">
          <h1>{loadedPost.title}</h1>
          <p>{loadedPost.body}</p>
          <div className="Edit">
            <button className="Delete" onClick={this.deletePostHandler}>
              Delete
            </button>
          </div>
        </div>
      );
    }
    return post;
  }
}

// FullPost.propTypes = {
//   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
// };

const fullPost = (props) => {
  // let post = <p style={{ textAlign: "center" }}>Please select a Post!</p>;
  // if (this.props.id) {
  //   post = <p style={{ textAlign: "center" }}>Loading...</p>;
  // }
  const [loadedPost, setLoadedPost] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!loadedPost || props.match.params.postId !== loadedPost.id) {
      setLoading(true);
      axios
        .get(`/posts/${props.match.params.postId}`)
        .then((res) => {
          setLoadedPost(res.data);
          setLoading(false);
        })
        .catch((error) => {
          console.log(error);
          setLoading(false);
        });
    }
  }, [props.match.params.postId]);

  const deletePostHandler = () => {
    axios.delete(`/posts/${props.match.params.postId}`).then((res) => {
      console.log(res);
    });
  };

  let post = null;
  if (loadedPost) {
    post = (
      <div className="FullPost">
        <h1>{loadedPost.title}</h1>
        <p>{loadedPost.body}</p>
        <div className="Edit">
          <button className="Delete" onClick={deletePostHandler}>
            Delete
          </button>
        </div>
      </div>
    );
  }
  if (loading) {
    post = <Spinner style={{ textAlign: "center" }}>Loading...</Spinner>;
  }
  return post;
};
// export default FullPost;
export default withErrorHandler(fullPost, axios);
