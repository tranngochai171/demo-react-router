import React from "react";
import PropTypes from "prop-types";
import "./Post.css";
import { withRouter } from "react-router-dom";

const post = (props) => (
  <article className="Post" onClick={props.clicked}>
    <h1>{props.post.title}</h1>
    <div className="Info">
      <div className="Author">{props.post.author}</div>
    </div>
  </article>
);

post.propTypes = {
  post: PropTypes.object.isRequired,
  clicked: PropTypes.func,
};

export default withRouter(post);
